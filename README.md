# C�digo Fluente Tube - Youtube React Native App
React Native App created for iOS, Android and Web with Youtube Google Data API to retrieve some videos according to the user's search.
<br/>
<p align="center">
  <img src="/assets/images/splash.png" width="300px" alt="React Native Youtube Search App" />
</p>
<br/>
[Run in virtual device now in snack expo io](https://snack.expo.io/@toticavalcanti/gitlab.com-toticavalcanti-youtube-api-codigo-fluente)

## The link above is to snack expo io, and you will see this page:

<br/>
![alt text](https://iili.io/fASWmu.png)

<br/>

## Click on the Tap to run in virtual device or Run on your device, to scan the code in your mobile device and run.

<br/>
![alt text](https://iili.io/fASVee.png)
<br/>

See the [demos on Snack/Expo](https://snack.expo.io/@appdevcanada/youtube-video-search?&preview=true&platform=ios&supportedPlatforms=android,ios,web).

## Quickstart

First, clone the repository:

```bash
git clone git@github.com:toticavalcanti/youtube-api-codigo-fluente.git
cd youtube-api-codigo-fluente

# install the dependencies
npm install

# start the server
expo start
```

## Configuring the Application

This app uses [Google Data API](https://developers.google.com/youtube/v3/?hl=en_GB) for Youtube content search. In order to test the application, you can use my API key, but for your own development, please register there and get yours. After getting the API key, go to `/config/API.js` and change the `API_KEY =` to your API key value.

## Starting the Application

```bash
expo start
```

This will open the browser with the available options to test the application.
Select iOS, Android or Web to open it in the simulator, or in your device. In order to run in simulators, you need to have them previously installed.

## Used Libraries

This app uses the following technologies/libraries:

* expo
* expo-screen-orientation
* native-base
* react
* react-native
* react-native-web
* react-native-webview
* And much much more

## Contributing

Feel free to clone this repository and contribute with more functionalities, new ideas and improvements.

```shell
git clone git@github.com:toticavalcanti/youtube-api-codigo-fluente.git
cd youtube-api-codigo-fluente
npm install
expo start
```

